#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
	int rv;

	if (argc < 2)
		abort();

	FILE *fp = fopen(argv[1], "r");
	if (!fp)
		abort();


	size_t size = 1;
	int *nums = calloc(size, sizeof(int));
	if (!nums)
		abort();
	size_t idx = 0;
	char *line = NULL;
	size_t len = 0;
	while (getline(&line, &len, fp) != -1) {
		if (idx == size) {
			size *= 2;
			nums = reallocarray(nums, size, sizeof(int));
			if (!nums)
				abort();
		}
		nums[idx] = atoi(line);
		idx++;
	}
	free(line);
	nums[idx] = 0;

	rv = fclose(fp);
	if (rv)
		abort();

	int num;
	for (int i = 0; (num = nums[i]); i++) {
		if (num > 1) {
			bool no_prime = false;
			for (int j = 2; j < num; j++) {
				if (num % j == 0) {
					printf("%i is not a prime number\n", num);
					no_prime = true;
					break;
				}
			}
			if (no_prime == false)
				printf("%i is a prime number\n", num);
		}
	}

	free(nums);
}
