use std::{env, fs};

fn main() {
    let nums = fs::read_to_string(env::args().nth(1).unwrap())
        .unwrap()
        .lines()
        .map(|s| s.parse::<i32>().unwrap())
        .collect::<Vec<_>>();

    for num in nums {
        if num > 1 {
            if (2..num).into_iter().find(|i| num % i == 0).is_some() {
                println!("{num} is not a prime number");
            } else {
                println!("{num} is a prime number");
            }
        }
    }
}
