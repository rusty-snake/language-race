#!/usr/bin/python3

import matplotlib.pyplot as plt

data = [0.384, 0.377, 1.787, 0.614, 23.873]
labels = ["Rust", "C", "Scala", "Java", "Python"]
plt.xticks(range(len(data)), labels)
plt.ylabel("Seconds")
plt.bar(range(len(data)), data)
plt.show()
