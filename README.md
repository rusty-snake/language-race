language-race
=============

Comparison of the speed of different programming languages.

```
rustc 1.59.0 (9d1b2106e 2022-02-23)
0,384s
gcc (GCC) 11.2.1 20220127 (Red Hat 11.2.1-9)
0,377s
Scala compiler version 3.1.2 -- Copyright 2002-2022, LAMP/EPFL
1,787s
javac 11.0.14.1
0,614s
Python 3.10.3
23,873s
```

![](results-fs8.png)

How was the test done?
----------------------

Look at `bench.sh` and the different implementations. ;)

Every programs check 10.000 numbers (range: 1-524287) whether
they are a prime number or not using a very dump algorithm.

Why is this a bad idea?
-----------------------

Because it has nothing to do with the real world, the real use cases
and the real computations of an applications.

If you for example check only 1.000 numbers in a range from 1 to 32767 Python
becomes twice as fast than Scala.

See also: <https://benchmarksgame-team.pages.debian.net/benchmarksgame/why-measure-toy-benchmark-programs.html>

License
-------

0BSD
