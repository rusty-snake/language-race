#!/bin/bash

export TIMEFORMAT='%Rs'

#[[ -e numbers.txt ]] || shuf -i 1-2147483647 -n 10000 > numbers.txt
[[ -e numbers.txt ]] || shuf -i 1-524287 -n 10000 > numbers.txt
#[[ -e numbers.txt ]] || shuf -i 1-65535 -n 10000 > numbers.txt
#[[ -e numbers.txt ]] || shuf -i 1-32767 -n 10000 > numbers.txt
#[[ -e numbers.txt ]] || shuf -i 1-32767 -n 1000 > numbers.txt

rustc --version |& tee -a log.txt
rustc --edition=2021 -O is_prime.rs
time ./is_prime numbers.txt >/dev/null |& tee -a log.txt

gcc --version | head -n1 |& tee -a log.txt
gcc -Wall -O2 -o is_prime is_prime.c
#gcc -Wall -fanalyzer -fsanitize=address,undefined -o is_prime is_prime.c
time ./is_prime numbers.txt >/dev/null |& tee -a log.txt

scalac --version |& tee -a log.txt
scalac is_prime.scala
time scala isPrime numbers.txt >/dev/null |& tee -a log.txt

javac --version |& tee -a log.txt
javac is_prime.java
time java is_prime numbers.txt >/dev/null |& tee -a log.txt

python3 --version |& tee -a log.txt
time python3 is_prime.py numbers.txt >/dev/null |& tee -a log.txt

rm is_prime *.class *.tasty
