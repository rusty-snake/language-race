import java.io.IOException;
import java.lang.Integer;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class is_prime {
    public static void main(String []args) throws IOException {
        List<Integer> nums = Files
            .readAllLines(Paths.get(args[0]))
            .stream()
            .map(Integer::parseInt)
            .collect(Collectors.toList());

        for (int num : nums) {
            if (IntStream.range(2, num).filter(i -> num % i == 0).findAny().isPresent()) {
                System.out.println(num + " is not a prime number");
            } else {
                System.out.println(num + " is a prime number");
            }
        }
    }
}
