#!/usr/bin/python3

import sys


def main():
    with open(sys.argv[1]) as numbers:
        nums = [int(num) for num in numbers]

    for num in nums:
        if num > 1:
            if next(filter(lambda i: num % i == 0, range(2, num)), None) is not None:
                print(f"{num} is not a prime number")
            else:
                print(f"{num} is a prime number")


main()
