def readToString(name: String): String =
  val bufferedSource = io.Source.fromFile(name)
  val string = bufferedSource.mkString
  bufferedSource.close
  string

@main def isPrime(numbers: String): Unit =
  val nums = readToString(numbers).linesIterator.map(_.toInt).toList

  for
    num <- nums
  do
    if num > 1 then
      if (2 to num).iterator.find(num % _ == 0).isDefined then
        println(s"$num is not a prime number")
      else
        println(s"$num is a prime number")
